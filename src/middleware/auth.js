import {getLocalStorage} from "../hellper/hellper.js";
import {pathname} from "../config/namespace-url.js";

const auth = (namespace) => {
    const authUser = getLocalStorage('access_token')
    if (pathname(namespace)){
        if (authUser.length === 0){
            window.location.replace("/auth/login");
        }
    }
    return false
}


export {auth}