const addCountCart = (productId = 0) => {
    const GetLocalStore = getLocalStorage("product_ids")

    if (productId){
        const sume =  GetLocalStore ? [...GetLocalStore, parseInt(productId)] : [parseInt(productId)]
        localStorage.setItem("product_ids", JSON.stringify(sume))
    }

    const countProductCart = document.getElementsByClassName("cookie-count-product")
    countProductCart[0].innerHTML = GetLocalStore.length +1

}


const getLocalStorage = (key) => {
    const localStor = localStorage.getItem(key)
    const GetLocalStore = [undefined, null].includes(localStor) ? '[]' : localStor
    return JSON.parse(GetLocalStore)
}

const addLocalStorage = (name, params) => {
    const checkParams = JSON.stringify(params) === undefined ? '[]' : JSON.stringify(params)
    localStorage.setItem(name, checkParams)
    return true
}


export { addCountCart, getLocalStorage, addLocalStorage }