import {GetDefLang, Url} from "../config/config-env.js";
import {getLocalStorage} from "./hellper.js";

const getRequest = async (url)  => {
    const option = {
            method: 'get',
            headers: new Headers({
                'Authorization': 'Bearer '+ getLocalStorage('access_token'),
                'Content-Type': 'application/json',
                'Localization': getLocalStorage('localization') ?? GetDefLang
            }),
        }

    const request =  await fetch(Url+url, option)

    const output = await request.json()
    return output
}



const postRequest = async (url, body)  => {
    const option = {
        method: 'post',
        headers: new Headers({
            'Authorization': 'Bearer '+ getLocalStorage('access_token'),
            'Content-Type': 'application/json',
            'Localization': getLocalStorage('localization') ?? GetDefLang
        }),
        body: JSON.stringify(body)
    }

    const request =  await fetch(Url+url, option)

    const output = await request.json()
    return output
}


export { getRequest, postRequest }