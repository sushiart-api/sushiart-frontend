import {pathname} from "./namespace-url.js";

const importCssBasedOnPathname = () => {
    // const hostname = window.location.hostname;

    const profileCssPates = [
        '/assets/css/all.min.css',
        '/profile/assets/css/nucleo-icons.css',
        '/profile/assets/css/nucleo-svg.css',
        '/profile/assets/css/soft-ui-dashboard.css',
        '/profile/assets/css/custom.css',
    ]
    const saitCssPates = [
        '/assets/css/all.min.css',
        '/assets/bootstrap/css/bootstrap.min.css',
        '/assets/css/magnific-popup.css',
        '/assets/css/animate.css',
        '/assets/css/meanmenu.min.css',
        '/assets/css/main.css',
        '/assets/css/responsive.css',
        '/global/style.css',
        '/custom/css/css.css',
        '/assets/js/main.js',
    ]

    if (pathname('profile') > 0 || pathname('auth') > 0) {
        saitCssPates.map((path)=>{
            $(`link[href="${path}"]`).remove();
        })
        profileCssPates.map((path)=>{
            if ($(`link[href="${path}"]`).length === 0){
                $('head').append(`<link href="${path}" rel="stylesheet">`)
            }
        })

    }else {
        profileCssPates.map((path)=>{
            $(`link[href="${path}"]`).remove();
        })
        saitCssPates.map((path)=>{
            if ($(`link[href="${path}"]`).length === 0){
                $('head').append(`<link href="${path}" rel="stylesheet">`)
            }
        })
    }
};


export { importCssBasedOnPathname }

