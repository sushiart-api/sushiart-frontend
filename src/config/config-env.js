const env = import.meta.env
const Url = import.meta.env.VITE_APP_URL

const UrlTest = import.meta.env.VITE_APP_URL_TEST

const GetDefLang = import.meta.env.VITE_APP_DEFAULT_LANG

const GetLang = import.meta.env.VITE_APP_LANG


export { env, Url, UrlTest, GetDefLang, GetLang}