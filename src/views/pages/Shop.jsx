import {useState, useEffect, useCallback} from "react";
import {Link} from "react-router-dom";
import { PreLoaderProduct } from "../components/PreLoader";
import Breadcrumb from "../components/Breadcrumb";
import {AddToCart} from "../components/AddToCart.jsx";
import {Url, UrlTest} from "../../config/config-env.js";
import {getRequest} from "../../hellper/db-request.js";
// import logo from "/vite.svg";


export default function Shop(){
    const [products, setProducts] = useState([]);
    const [categories, setCategories] = useState([]);
    const [loading, setLoading] = useState(false)
    const [tab, setTab] = useState()



    const fetchCategories = async () => {
        const loadedLangs = await getRequest('/shop/category')
        setCategories(loadedLangs);
        setTab(loadedLangs[0].id)
    };

    useEffect(() => {
        fetchCategories();
    }, [])


    const changeCategory = async () => {
        setLoading(true)
        if (tab !== undefined){
            const loadedLangs = await getRequest(`/shop/products/${tab}`)
            setProducts(loadedLangs)
        }
        setLoading(false)
    };

    useEffect(() => {
        changeCategory(tab)
    }, [tab])


    return(
        <>
            <Breadcrumb subtitle={'Fresh and Organic'} title={'Shop'} path={'/assets/img/hero-bg.jpg'}/>

                <>
                    <div className="product-section mt-150 mb-150">
                        <div className="container">

                            <div className="row">
                                <div className="col-md-12">
                                    <div className="product-filters">
                                        <ul>
                                            {
                                                categories.map((category) => (
                                                    <li key={category.id} onClick={() => setTab(category.id)} className={tab === category.id ? `active` : ''}>{category.title}</li>
                                                ))
                                            }
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {loading && <PreLoaderProduct />}
                            {!loading && (
                                <>
                                    <div className="row product-lists">

                                        {
                                            products.map((product) => (
                                                <div key={product.id}
                                                     className={`col-lg-4 col-md-6 text-center ${product.category_name}`}>
                                                    <div className="single-product-item">
                                                        <Link to={`/product/${product.id}`}>
                                                            <div className="product-image">
                                                                <img src={product.image} alt=""/>
                                                            </div>
                                                        <h3>{product.title}</h3>
                                                        </Link>
                                                        <p className="product-price">
                                                            <span>{product.sale} ֏</span>
                                                            {product.price} ֏
                                                        </p>
                                                        <AddToCart id={product.id}/>
                                                    </div>
                                                </div>
                                            ))
                                        }

                                    </div>

                                    {/*<div className="row">*/}
                                    {/*    <div className="col-lg-12 text-center">*/}
                                    {/*        <div className="pagination-wrap">*/}
                                    {/*            <ul>*/}
                                    {/*                <li><Link to="#">Prev</Link></li>*/}
                                    {/*                <li><Link to="#">1</Link></li>*/}
                                    {/*                <li><Link className="active" to="#">2</Link></li>*/}
                                    {/*                <li><Link to="#">3</Link></li>*/}
                                    {/*                <li><Link to="#">Next</Link></li>*/}
                                    {/*            </ul>*/}
                                    {/*        </div>*/}
                                    {/*    </div>*/}
                                    {/*</div>*/}
                                </>
                            )}
                        </div>
                    </div>
                </>

        </>

    )
}