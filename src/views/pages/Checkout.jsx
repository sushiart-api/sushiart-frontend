import Breadcrumb from "../components/Breadcrumb.jsx";
import {Link} from "react-router-dom";
import {useCallback, useEffect, useState} from "react";
import {UrlTest} from "../../config/config-env.js";
import {PreLoaderProduct} from "../components/PreLoader.jsx";

export default function Checkout(){

    const [cart, setCart] = useState()
    const [loading, setLoading] = useState(false)

    const fetchCart = useCallback( async ()=>{
        setLoading(true)
        const responce = await fetch(UrlTest+'/carts/1')
        const cart = await responce.json()
        setCart(cart)
        setLoading(false)
    }, [])


    useEffect(()=>{
        fetchCart()
    }, [fetchCart])

    console.log(cart)




    return(
        <>
            <Breadcrumb subtitle={'Fresh and Organic'} title={'Checkout'} path={'/assets/img/hero-bg.jpg'}/>
            {loading && <PreLoaderProduct />}
            {!loading && (
                <div className="checkout-section mt-150 mb-150">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="checkout-accordion-wrap">
                                    <div className="accordion" id="accordionExample">
                                        <div className="card single-accordion">
                                            <div className="card-header" id="headingOne">
                                                <h5 className="mb-0">
                                                    <button className="btn btn-link" type="button" data-toggle="collapse"
                                                            data-target="#collapseOne" aria-expanded="true"
                                                            aria-controls="collapseOne">
                                                        Billing Address
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" className="collapse show" aria-labelledby="headingOne"
                                                 data-parent="#accordionExample">
                                                <div className="card-body">
                                                    <div className="billing-address-form">
                                                        <form action="index.html">
                                                            <p>
                                                                <input type="text" placeholder="Name*" required={true}/>
                                                            </p>
                                                            <p>
                                                                <input type="email" placeholder="Email"/>
                                                            </p>
                                                            <p>
                                                                <input type="text" placeholder="City" value={'Yerevan'}
                                                                       disabled={true}/>
                                                            </p>
                                                            <p>
                                                                <input type="text" placeholder="Address*"  required={true}/>
                                                            </p>
                                                            <p>
                                                                <input type="tel" placeholder="Phone*"  required={true}/>
                                                            </p>
                                                            <p>
                                                                <span className="toggle">
                                                                    <input type="radio" name={'pay_method'} value={'card'}
                                                                           id="sizeWeight" defaultChecked={true}/>
                                                                    <label htmlFor="sizeWeight" style={{outline: 'none'}}>
                                                                        card
                                                                    </label>
                                                                    <input type="radio" name={'pay_method'} value={"cash"}
                                                                           id="sizeDimensions"/>
                                                                    <label htmlFor="sizeDimensions"
                                                                           style={{outline: 'none'}}>
                                                                        cash
                                                                    </label>
                                                                </span>
                                                            </p>
                                                            <p>
                                                                <textarea name="location_url" id="bill" cols="30" rows="10"
                                                                          placeholder="Location url"></textarea>
                                                            </p>
                                                            <p>
                                                                <textarea name="bill" id="bill" cols="30" rows="10"
                                                                          placeholder="Say Something"></textarea>
                                                            </p>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/*<div className="card single-accordion">*/}
                                        {/*    <div className="card-header" id="headingTwo">*/}
                                        {/*        <h5 className="mb-0">*/}
                                        {/*            <button className="btn btn-link collapsed" type="button"*/}
                                        {/*                    data-toggle="collapse" data-target="#collapseTwo"*/}
                                        {/*                    aria-expanded="false" aria-controls="collapseTwo">*/}
                                        {/*                Shipping Address*/}
                                        {/*            </button>*/}
                                        {/*        </h5>*/}
                                        {/*    </div>*/}
                                        {/*    <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo"*/}
                                        {/*         data-parent="#accordionExample">*/}
                                        {/*        <div className="card-body">*/}
                                        {/*            <div className="shipping-address-form">*/}
                                        {/*                <p>Your shipping address form is here.</p>*/}
                                        {/*            </div>*/}
                                        {/*        </div>*/}
                                        {/*    </div>*/}
                                        {/*</div>*/}
                                        {/*<div className="card single-accordion">*/}
                                        {/*    <div className="card-header" id="headingThree">*/}
                                        {/*        <h5 className="mb-0">*/}
                                        {/*            <button className="btn btn-link collapsed" type="button"*/}
                                        {/*                    data-toggle="collapse" data-target="#collapseThree"*/}
                                        {/*                    aria-expanded="false" aria-controls="collapseThree">*/}
                                        {/*                Card Details*/}
                                        {/*            </button>*/}
                                        {/*        </h5>*/}
                                        {/*    </div>*/}
                                        {/*    <div id="collapseThree" className="collapse" aria-labelledby="headingThree"*/}
                                        {/*         data-parent="#accordionExample">*/}
                                        {/*        <div className="card-body">*/}
                                        {/*            <div className="card-details">*/}
                                        {/*                <p>Your card details goes here.</p>*/}
                                        {/*            </div>*/}
                                        {/*        </div>*/}
                                        {/*    </div>*/}
                                        {/*</div>*/}
                                    </div>

                                </div>
                            </div>

                            <div className="col-lg-4">
                                <div className="order-details-wrap">
                                    <table className="order-details">
                                        <thead>
                                        <tr>
                                            <th>Your order Details</th>
                                            <th>Price</th>
                                        </tr>
                                        </thead>
                                        <tbody className="order-details-body">
                                        {cart?.products?.map((product) => (
                                            <tr key={product?.id}>
                                                <td>{product?.title}</td>
                                                <td>${product?.total}</td>
                                            </tr>
                                        ))
                                        }
                                        </tbody>
                                        <tbody className="checkout-details" style={{border: '3px solid #bdbdbd'}}>
                                        <tr>
                                            <td>Subtotal</td>
                                            <td>${cart?.total}</td>
                                        </tr>
                                        <tr>
                                            <td>Shipping</td>
                                            <td>{cart?.total < 5000 ? '~//~' : '$0'}</td>
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td>${cart?.total}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <Link to="#" className="boxed-btn">Place Order</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    )
}