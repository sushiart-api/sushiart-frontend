import {Link} from "react-router-dom";


export default function Account(){

    return(
        <>
            <div className="container-fluid">
                <div className="page-header min-height-300 border-radius-xl mt-4"
                     style={{backgroundImage: "url('/profile/assets/img/curved-images/curved0.jpg')", backgroundPositionY: '50%'}}>
                    <span className="mask bg-gradient-primary opacity-6"></span>
                </div>
                <div className="card card-body blur shadow-blur mx-4 mt-n6 overflow-hidden">
                    <div className="row gx-4">
                        <div className="col-auto">
                            <div className="avatar avatar-xl position-relative">
                                <img src={'/profile/assets/img/bruce-mars.jpg'} alt="profile_image"
                                     className="w-100 border-radius-lg shadow-sm"/>
                            </div>
                        </div>
                        <div className="col-auto my-auto">
                            <div className="h-100">
                                <h5 className="mb-1">
                                    Alec Thompson
                                </h5>
                                <p className="mb-0 font-weight-bold text-sm">
                                    CEO / Co-Founder
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container-fluid py-4">
                <div className="row mb-5">
                    <div className="col-md-7 mt-4">
                        <div className="card">
                            <div className="card-header pb-0 px-3">
                                <h6 className="mb-0">Billing Information</h6>
                            </div>
                            <div className="card-body pt-4 p-3">
                                <ul className="list-group">
                                    <li className="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                        <div className="d-flex flex-column">
                                            <h6 className="mb-3 text-sm">Oliver Liam</h6>
                                            <span className="mb-2 text-xs">Company Name: <span
                                                className="text-dark font-weight-bold ms-sm-2">Viking Burrito</span></span>
                                            <span className="mb-2 text-xs">Email Address: <span
                                                className="text-dark ms-sm-2 font-weight-bold">oliver@burrito.com</span></span>
                                            <span className="text-xs">VAT Number: <span
                                                className="text-dark ms-sm-2 font-weight-bold">FRB1235476</span></span>
                                        </div>
                                        <div className="ms-auto text-end">
                                            <Link className="btn btn-link text-danger text-gradient px-3 mb-0"
                                               to="#"><i className="far fa-trash-alt me-2"></i>Delete</Link>
                                            <Link className="btn btn-link text-dark px-3 mb-0" to="#"><i
                                                className="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</Link>
                                        </div>
                                    </li>
                                    <li className="list-group-item border-0 d-flex p-4 mb-2 mt-3 bg-gray-100 border-radius-lg">
                                        <div className="d-flex flex-column">
                                            <h6 className="mb-3 text-sm">Lucas Harper</h6>
                                            <span className="mb-2 text-xs">Company Name: <span
                                                className="text-dark font-weight-bold ms-sm-2">Stone Tech Zone</span></span>
                                            <span className="mb-2 text-xs">Email Address: <span
                                                className="text-dark ms-sm-2 font-weight-bold">lucas@stone-tech.com</span></span>
                                            <span className="text-xs">VAT Number: <span
                                                className="text-dark ms-sm-2 font-weight-bold">FRB1235476</span></span>
                                        </div>
                                        <div className="ms-auto text-end">
                                            <Link className="btn btn-link text-danger text-gradient px-3 mb-0"
                                               to="#"><i className="far fa-trash-alt me-2"></i>Delete</Link>
                                            <Link className="btn btn-link text-dark px-3 mb-0" to="#"><i
                                                className="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</Link>
                                        </div>
                                    </li>
                                    <li className="list-group-item border-0 d-flex p-4 mb-2 mt-3 bg-gray-100 border-radius-lg">
                                        <div className="d-flex flex-column">
                                            <h6 className="mb-3 text-sm">Ethan James</h6>
                                            <span className="mb-2 text-xs">Company Name: <span
                                                className="text-dark font-weight-bold ms-sm-2">Fiber Notion</span></span>
                                            <span className="mb-2 text-xs">Email Address: <span
                                                className="text-dark ms-sm-2 font-weight-bold">ethan@fiber.com</span></span>
                                            <span className="text-xs">VAT Number: <span
                                                className="text-dark ms-sm-2 font-weight-bold">FRB1235476</span></span>
                                        </div>
                                        <div className="ms-auto text-end">
                                            <Link className="btn btn-link text-danger text-gradient px-3 mb-0"
                                               to="#"><i className="far fa-trash-alt me-2"></i>Delete</Link>
                                            <Link className="btn btn-link text-dark px-3 mb-0" to="#"><i
                                                className="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</Link>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-5 mt-4">
                        <div className="card h-100 mb-4">
                            <div className="card-header pb-0 px-3">
                                <div className="row">
                                    <div className="col-md-6">
                                        <h6 className="mb-0">Your Transactions</h6>
                                    </div>
                                    <div className="col-md-6 d-flex justify-content-end align-items-center">
                                        <i className="far fa-calendar-alt me-2"></i>
                                        <small>23 - 30 March 2020</small>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body pt-4 p-3">
                                <h6 className="text-uppercase text-body text-xs font-weight-bolder mb-3">Newest</h6>
                                <ul className="list-group">
                                    <li className="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                        <div className="d-flex align-items-center">
                                            <button
                                                className="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 btn-sm d-flex align-items-center justify-content-center">
                                                <i className="fas fa-arrow-down"></i></button>
                                            <div className="d-flex flex-column">
                                                <h6 className="mb-1 text-dark text-sm">Netflix</h6>
                                                <span className="text-xs">27 March 2020, at 12:30 PM</span>
                                            </div>
                                        </div>
                                        <div
                                            className="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold">
                                            - $ 2,500
                                        </div>
                                    </li>
                                    <li className="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                        <div className="d-flex align-items-center">
                                            <button
                                                className="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 btn-sm d-flex align-items-center justify-content-center">
                                                <i className="fas fa-arrow-up"></i></button>
                                            <div className="d-flex flex-column">
                                                <h6 className="mb-1 text-dark text-sm">Apple</h6>
                                                <span className="text-xs">27 March 2020, at 04:30 AM</span>
                                            </div>
                                        </div>
                                        <div
                                            className="d-flex align-items-center text-success text-gradient text-sm font-weight-bold">
                                            + $ 2,000
                                        </div>
                                    </li>
                                </ul>
                                <h6 className="text-uppercase text-body text-xs font-weight-bolder my-3">Yesterday</h6>
                                <ul className="list-group">
                                    <li className="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                        <div className="d-flex align-items-center">
                                            <button
                                                className="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 btn-sm d-flex align-items-center justify-content-center">
                                                <i className="fas fa-arrow-up"></i></button>
                                            <div className="d-flex flex-column">
                                                <h6 className="mb-1 text-dark text-sm">Stripe</h6>
                                                <span className="text-xs">26 March 2020, at 13:45 PM</span>
                                            </div>
                                        </div>
                                        <div
                                            className="d-flex align-items-center text-success text-gradient text-sm font-weight-bold">
                                            + $ 750
                                        </div>
                                    </li>
                                    <li className="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                        <div className="d-flex align-items-center">
                                            <button
                                                className="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 btn-sm d-flex align-items-center justify-content-center">
                                                <i className="fas fa-arrow-up"></i></button>
                                            <div className="d-flex flex-column">
                                                <h6 className="mb-1 text-dark text-sm">HubSpot</h6>
                                                <span className="text-xs">26 March 2020, at 12:30 PM</span>
                                            </div>
                                        </div>
                                        <div
                                            className="d-flex align-items-center text-success text-gradient text-sm font-weight-bold">
                                            + $ 1,000
                                        </div>
                                    </li>
                                    <li className="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                        <div className="d-flex align-items-center">
                                            <button
                                                className="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 btn-sm d-flex align-items-center justify-content-center">
                                                <i className="fas fa-arrow-up"></i></button>
                                            <div className="d-flex flex-column">
                                                <h6 className="mb-1 text-dark text-sm">Creative Tim</h6>
                                                <span className="text-xs">26 March 2020, at 08:30 AM</span>
                                            </div>
                                        </div>
                                        <div
                                            className="d-flex align-items-center text-success text-gradient text-sm font-weight-bold">
                                            + $ 2,500
                                        </div>
                                    </li>
                                    <li className="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                        <div className="d-flex align-items-center">
                                            <button
                                                className="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 btn-sm d-flex align-items-center justify-content-center">
                                                <i className="fas fa-exclamation"></i></button>
                                            <div className="d-flex flex-column">
                                                <h6 className="mb-1 text-dark text-sm">Webflow</h6>
                                                <span className="text-xs">26 March 2020, at 05:00 AM</span>
                                            </div>
                                        </div>
                                        <div className="d-flex align-items-center text-dark text-sm font-weight-bold">
                                            Pending
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-xl-4">
                        <div className="card h-100">
                            <div className="card-header pb-0 p-3">
                                <h6 className="mb-0">Platform Settings</h6>
                            </div>
                            <div className="card-body p-3">
                                <h6 className="text-uppercase text-body text-xs font-weight-bolder">Account</h6>
                                <ul className="list-group">
                                    <li className="list-group-item border-0 px-0">
                                        <div className="form-check form-switch ps-0">
                                            <input className="form-check-input ms-auto" type="checkbox"
                                                   id="flexSwitchCheckDefault" defaultChecked/>
                                            <label className="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                                   htmlFor="flexSwitchCheckDefault">Email me when someone follows
                                                me</label>
                                        </div>
                                    </li>
                                    <li className="list-group-item border-0 px-0">
                                        <div className="form-check form-switch ps-0">
                                            <input className="form-check-input ms-auto" type="checkbox"
                                                   id="flexSwitchCheckDefault1"/>
                                            <label className="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                                   htmlFor="flexSwitchCheckDefault1">Email me when someone answers on my
                                                post</label>
                                        </div>
                                    </li>
                                    <li className="list-group-item border-0 px-0">
                                        <div className="form-check form-switch ps-0">
                                            <input className="form-check-input ms-auto" type="checkbox"
                                                   id="flexSwitchCheckDefault2" defaultChecked/>
                                            <label className="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                                   htmlFor="flexSwitchCheckDefault2">Email me when someone mentions
                                                me</label>
                                        </div>
                                    </li>
                                </ul>
                                <h6 className="text-uppercase text-body text-xs font-weight-bolder mt-4">Application</h6>
                                <ul className="list-group">
                                    <li className="list-group-item border-0 px-0">
                                        <div className="form-check form-switch ps-0">
                                            <input className="form-check-input ms-auto" type="checkbox"
                                                   id="flexSwitchCheckDefault3"/>
                                            <label className="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                                   htmlFor="flexSwitchCheckDefault3">New launches and projects</label>
                                        </div>
                                    </li>
                                    <li className="list-group-item border-0 px-0">
                                        <div className="form-check form-switch ps-0">
                                            <input className="form-check-input ms-auto" type="checkbox"
                                                   id="flexSwitchCheckDefault4" defaultChecked/>
                                            <label className="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                                   htmlFor="flexSwitchCheckDefault4">Monthly product updates</label>
                                        </div>
                                    </li>
                                    <li className="list-group-item border-0 px-0 pb-0">
                                        <div className="form-check form-switch ps-0">
                                            <input className="form-check-input ms-auto" type="checkbox"
                                                   id="flexSwitchCheckDefault5"/>
                                            <label className="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                                   htmlFor="flexSwitchCheckDefault5">Subscribe to newsletter</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-xl-4">
                        <div className="card h-100">
                            <div className="card-header pb-0 p-3">
                                <div className="row">
                                    <div className="col-md-8 d-flex align-items-center">
                                        <h6 className="mb-0">Profile Information</h6>
                                    </div>
                                    <div className="col-md-4 text-end">
                                        <Link to="#">
                                            <i className="fas fa-user-edit text-secondary text-sm"
                                               data-bs-toggle="tooltip" data-bs-placement="top"
                                               title="Edit Profile"></i>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body p-3">
                                <p className="text-sm">
                                    Hi, I’m Alec Thompson, Decisions: If you can’t decide, the answer is no. If two
                                    equally difficult paths, choose the one more painful in the short term (pain
                                    avoidance is creating an illusion of equality).
                                </p>
                                <hr className="horizontal gray-light my-4"/>
                                <ul className="list-group">
                                    <li className="list-group-item border-0 ps-0 pt-0 text-sm"><strong
                                        className="text-dark">Full Name:</strong> &nbsp; Alec M. Thompson
                                    </li>
                                    <li className="list-group-item border-0 ps-0 text-sm"><strong
                                        className="text-dark">Mobile:</strong> &nbsp; (44) 123 1234 123
                                    </li>
                                    <li className="list-group-item border-0 ps-0 text-sm"><strong
                                        className="text-dark">Email:</strong> &nbsp; alecthompson@mail.com
                                    </li>
                                    <li className="list-group-item border-0 ps-0 text-sm"><strong
                                        className="text-dark">Location:</strong> &nbsp; USA
                                    </li>
                                    <li className="list-group-item border-0 ps-0 pb-0">
                                        <strong className="text-dark text-sm">Social:</strong> &nbsp;
                                        <Link className="btn btn-facebook btn-simple mb-0 ps-1 pe-2 py-0"
                                              to="#">
                                            <i className="fab fa-facebook fa-lg"></i>
                                        </Link>
                                        <Link className="btn btn-twitter btn-simple mb-0 ps-1 pe-2 py-0"
                                              to="#">
                                            <i className="fab fa-twitter fa-lg"></i>
                                        </Link>
                                        <Link className="btn btn-instagram btn-simple mb-0 ps-1 pe-2 py-0"
                                              to="#">
                                            <i className="fab fa-instagram fa-lg"></i>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-xl-4">
                        <div className="card h-100">
                            <div className="card-header pb-0 p-3">
                                <h6 className="mb-0">Conversations</h6>
                            </div>
                            <div className="card-body p-3">
                                <ul className="list-group">
                                    <li className="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                                        <div className="avatar me-3">
                                            <img src="/profile/assets/img/kal-visuals-square.jpg" alt="kal"
                                                 className="border-radius-lg shadow"/>
                                        </div>
                                        <div className="d-flex align-items-start flex-column justify-content-center">
                                            <h6 className="mb-0 text-sm">Sophie B.</h6>
                                            <p className="mb-0 text-xs">Hi! I need more information..</p>
                                        </div>
                                        <Link className="btn btn-link pe-3 ps-0 mb-0 ms-auto" to="#">Reply</Link>
                                    </li>
                                    <li className="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                                        <div className="avatar me-3">
                                            <img src="/profile/assets/img/marie.jpg" alt="kal"
                                                 className="border-radius-lg shadow"/>
                                        </div>
                                        <div className="d-flex align-items-start flex-column justify-content-center">
                                            <h6 className="mb-0 text-sm">Anne Marie</h6>
                                            <p className="mb-0 text-xs">Awesome work, can you..</p>
                                        </div>
                                        <Link className="btn btn-link pe-3 ps-0 mb-0 ms-auto" to="#">Reply</Link>
                                    </li>
                                    <li className="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                                        <div className="avatar me-3">
                                            <img src="/profile/assets/img/ivana-square.jpg" alt="kal"
                                                 className="border-radius-lg shadow"/>
                                        </div>
                                        <div className="d-flex align-items-start flex-column justify-content-center">
                                            <h6 className="mb-0 text-sm">Ivanna</h6>
                                            <p className="mb-0 text-xs">About files I can..</p>
                                        </div>
                                        <Link className="btn btn-link pe-3 ps-0 mb-0 ms-auto" to="#">Reply</Link>
                                    </li>
                                    <li className="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                                        <div className="avatar me-3">
                                            <img src="/profile/assets/img/team-4.jpg" alt="kal"
                                                 className="border-radius-lg shadow"/>
                                        </div>
                                        <div className="d-flex align-items-start flex-column justify-content-center">
                                            <h6 className="mb-0 text-sm">Peterson</h6>
                                            <p className="mb-0 text-xs">Have a great afternoon..</p>
                                        </div>
                                        <Link className="btn btn-link pe-3 ps-0 mb-0 ms-auto" to="#">Reply</Link>
                                    </li>
                                    <li className="list-group-item border-0 d-flex align-items-center px-0">
                                        <div className="avatar me-3">
                                            <img src="/profile/assets/img/team-3.jpg" alt="kal"
                                                 className="border-radius-lg shadow"/>
                                        </div>
                                        <div className="d-flex align-items-start flex-column justify-content-center">
                                            <h6 className="mb-0 text-sm">Nick Daniel</h6>
                                            <p className="mb-0 text-xs">Hi! I need more information..</p>
                                        </div>
                                        <Link className="btn btn-link pe-3 ps-0 mb-0 ms-auto" to="#">Reply</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 mt-4">
                        <div className="card mb-4">
                            <div className="card-header pb-0 p-3">
                                <h6 className="mb-1">Projects</h6>
                                <p className="text-sm">Architects design houses</p>
                            </div>
                            <div className="card-body p-3">
                                <div className="row">
                                    <div className="col-xl-3 col-md-6 mb-xl-0 mb-4">
                                        <div className="card card-blog card-plain">
                                            <div className="position-relative">
                                                <Link className="d-block shadow-xl border-radius-xl" to="#">
                                                    <img src="/profile/assets/img/home-decor-1.jpg"
                                                         alt="img-blur-shadow"
                                                         className="img-fluid shadow border-radius-xl"/>
                                                </Link>
                                            </div>
                                            <div className="card-body px-1 pb-0">
                                                <p className="text-gradient text-dark mb-2 text-sm">Project #2</p>
                                                <Link to="#">
                                                    <h5>
                                                        Modern
                                                    </h5>
                                                </Link>
                                                <p className="mb-4 text-sm">
                                                    As Uber works through a huge amount of internal management turmoil.
                                                </p>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <button type="button"
                                                            className="btn btn-outline-primary btn-sm mb-0">View Project
                                                    </button>
                                                    <div className="avatar-group mt-2">
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Elena Morison">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-1.jpg"/>
                                                        </Link>
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Ryan Milly">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-2.jpg"/>
                                                        </Link>
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Nick Daniel">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-3.jpg"/>
                                                        </Link>
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Peterson">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-4.jpg"/>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-md-6 mb-xl-0 mb-4">
                                        <div className="card card-blog card-plain">
                                            <div className="position-relative">
                                                <Link className="d-block shadow-xl border-radius-xl" to="#">
                                                    <img src="/profile/assets/img/home-decor-2.jpg"
                                                         alt="img-blur-shadow"
                                                         className="img-fluid shadow border-radius-lg"/>
                                                </Link>
                                            </div>
                                            <div className="card-body px-1 pb-0">
                                                <p className="text-gradient text-dark mb-2 text-sm">Project #1</p>
                                                <Link to="#">
                                                    <h5>
                                                        Scandinavian
                                                    </h5>
                                                </Link>
                                                <p className="mb-4 text-sm">
                                                    Music is something that every person has his or her own specific
                                                    opinion about.
                                                </p>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <button type="button"
                                                            className="btn btn-outline-primary btn-sm mb-0">View Project
                                                    </button>
                                                    <div className="avatar-group mt-2">
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Nick Daniel">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-3.jpg"/>
                                                        </Link>
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Peterson">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-4.jpg"/>
                                                        </Link>
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Elena Morison">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-1.jpg"/>
                                                        </Link>
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Ryan Milly">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-2.jpg"/>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-md-6 mb-xl-0 mb-4">
                                        <div className="card card-blog card-plain">
                                            <div className="position-relative">
                                                <Link className="d-block shadow-xl border-radius-xl" to="#">
                                                    <img src="/profile/assets/img/home-decor-3.jpg"
                                                         alt="img-blur-shadow"
                                                         className="img-fluid shadow border-radius-xl"/>
                                                </Link>
                                            </div>
                                            <div className="card-body px-1 pb-0">
                                                <p className="text-gradient text-dark mb-2 text-sm">Project #3</p>
                                                <Link to="#">
                                                    <h5>
                                                        Minimalist
                                                    </h5>
                                                </Link>
                                                <p className="mb-4 text-sm">
                                                    Different people have different taste, and various types of music.
                                                </p>
                                                <div className="d-flex align-items-center justify-content-between">
                                                    <button type="button"
                                                            className="btn btn-outline-primary btn-sm mb-0">View Project
                                                    </button>
                                                    <div className="avatar-group mt-2">
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Peterson">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-4.jpg"/>
                                                        </Link>
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Nick Daniel">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-3.jpg"/>
                                                        </Link>
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Ryan Milly">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-2.jpg"/>
                                                        </Link>
                                                        <Link to="#"
                                                              className="avatar avatar-xs rounded-circle"
                                                              data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                              title="Elena Morison">
                                                            <img alt="Image placeholder"
                                                                 src="/profile/assets/img/team-1.jpg"/>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-md-6 mb-xl-0 mb-4">
                                        <div className="card h-100 card-plain border">
                                            <div
                                                className="card-body d-flex flex-column justify-content-center text-center">
                                                <Link to="#">
                                                    <i className="fa fa-plus text-secondary mb-3"></i>
                                                    <h5 className=" text-secondary"> New project </h5>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}