import {Link} from "react-router-dom";
import OwlCarousel from "react-owl-carousel";
import {useCallback, useEffect, useState} from "react";
import {UrlTest} from "../../../config/config-env.js";


export default function Order(){

    const [cart, setCart] = useState()

    const fetchCart = useCallback( async ()=>{
        // setLoading(true)
        const responce = await fetch(UrlTest+'/carts?limit=5')
        const cart = await responce.json()
        setCart(cart.carts)
        // setLoading(false)
    }, [])



    useEffect(()=>{
        fetchCart()
    }, [fetchCart])

    return(
        <>
            <div className="row">
                <div className="col-12">
                    <div className="card mb-4">
                        <div className="card-header pb-0">
                            <h6>Orders</h6>
                        </div>
                        <div className="card-body px-0 pt-0 pb-2">
                            <div className="table-responsive p-0">
                                <table className="table align-items-center mb-0">
                                    <thead>
                                    <tr>
                                        <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Products</th>
                                        <th className="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Total</th>
                                        <th className="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                                        <th className="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Employed</th>
                                        <th className="text-secondary opacity-7"></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        {
                                            cart?.map((order) => (
                                                <tr key={order.id}>
                                                    <td  className='product-images-inline'>
                                                        <OwlCarousel items={8} autoplay className="avatar-group mt-2">
                                                            {
                                                                order?.products.map((product) => (
                                                                    <Link key={product.id} to={`/product/${product.id}`} className="avatar avatar-xl rounded-circle"
                                                                          data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                                          title={product.title}>
                                                                        <img src={product.thumbnail} alt="team1" className='product-image-order'/>
                                                                    </Link>
                                                                ))
                                                            }
                                                        </OwlCarousel>
                                                    </td>
                                                    <td>
                                                        <p className="text-xs font-weight-bold mb-0">{order.total} $</p>
                                                        <p className="text-xs text-secondary mb-0">Organization</p>
                                                    </td>
                                                    <td className="align-middle text-center text-sm">
                                                        <span className="badge badge-sm bg-gradient-secondary">Pending</span>
                                                        <span className="badge badge-sm bg-gradient-primary">In process</span>
                                                        <span className="badge badge-sm bg-gradient-info"> Done </span>
                                                        <span className="badge badge-sm bg-gradient-warning">Sent</span>
                                                        <span className="badge badge-sm bg-gradient-success">Finished</span>
                                                        <span className="badge badge-sm bg-gradient-danger"> Canceled </span>
                                                    </td>
                                                    <td className="align-middle text-center">
                                                    <span className="text-secondary text-xs font-weight-bold">23/04/18</span>
                                                    </td>
                                                    <td className="align-middle">
                                                        <Link to="#"
                                                              className="font-weight-bold text-xs order-again"
                                                              data-toggle="tooltip" data-original-title="Edit user">
                                                            Order Again
                                                        </Link>
                                                    </td>
                                                </tr>
                                            ))
                                        }

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}