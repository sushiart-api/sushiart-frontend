import {Link} from "react-router-dom";
import Breadcrumb from "../components/Breadcrumb.jsx";
import {useCallback, useEffect, useState} from "react";
import {UrlTest} from "../../config/config-env.js";
import {addCountCart} from "../../hellper/hellper.js";

export default function Cart(){

    const [cart, setCart] = useState()
    const [total, setTotal] = useState()


    const fetchCart = useCallback( async ()=>{
        // setLoading(true)
        const responce = await fetch(UrlTest+'/carts/1')
        const cart = await responce.json()
        setCart(cart.products)
        totalPrice(cart.products)
        // setLoading(false)
    }, [])


    useEffect(()=>{
        fetchCart()
    }, [fetchCart])

    // console.log(cart, input)

    function changeQuantity(event){
        const id = event.target.attributes.id.value;
        const value = event.target.value;

        const  arr = cart.filter(function(item){
            if (item.id === parseInt(id)){
                item.quantity = parseInt(value);
                item.total = parseInt(item.price)*parseInt(item.quantity);
            }
            return item;
        });
        setCart(arr)


        addCountCart()
        totalPrice(cart)
    }

    function totalPrice(cart){
        const sum = cart.reduce(function(prev, current) {
            return prev + +current.total
        }, 0);
        setTotal(sum)
    }

    function remove(itemId){
        const arr = cart.filter(({ id }) => id !== itemId)
        setCart(arr)
        totalPrice(arr)
    }




    return(
        <>
            <Breadcrumb subtitle={'Fresh and Organic'} title={'Cart'} path={'/assets/img/hero-bg.jpg'}/>

            <div className="cart-section mt-150 mb-150">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-md-12">
                            <div className="cart-table-wrap">
                                <table className="cart-table">
                                    <thead className="cart-table-head">
                                    <tr className="table-head-row">
                                        <th className="product-remove"></th>
                                        <th className="product-image">Product Image</th>
                                        <th className="product-name">Name</th>
                                        <th className="product-price">Price</th>
                                        <th className="product-quantity">Quantity</th>
                                        <th className="product-total">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {cart?.map((product)=> (
                                        <tr key={product.id} className="table-body-row">
                                            <td className="product-remove">
                                                <span style={{cursor: 'pointer'}} onClick={()=>remove(product.id)} >
                                                    <i style={{color: 'red'}} className="far fa-trash-alt"></i>
                                                </span>
                                            </td>
                                            <td className="product-image">
                                                <img src={product.thumbnail}
                                                     style={{maxWidth: '100%'}} alt=""/>
                                            </td>
                                            <td className="product-name">{product.title}</td>
                                            <td className="product-price">${product.price}</td>
                                            <td className="product-quantity">
                                                <input type="number" min={1} placeholder="0" id={product.id} onChange={changeQuantity} value={product.quantity}/>
                                            </td>
                                            <td className="product-total">{product.total} $</td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div className="col-lg-4">
                            <div className="total-section">
                                <table className="total-table">
                                    <thead className="total-table-head">
                                    <tr className="table-total-row">
                                        <th>Total</th>
                                        <th>Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr className="total-data">
                                        <td><strong>Subtotal: </strong></td>
                                        <td>${total}</td>
                                    </tr>
                                    <tr className="total-data">
                                        <td><strong>Shipping: </strong></td>
                                        <td>{total < 5000 ? '~//~' : '$0'}</td>
                                    </tr>
                                    <tr className="total-data">
                                        <td><strong>Total: </strong></td>
                                        <td>${total}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div className="cart-buttons">
                                    <Link to="/checkout" className="boxed-btn black">Check Out</Link>
                                </div>
                            </div>

                            {/*<div className="coupon-section">*/}
                            {/*    <h3>Apply Coupon</h3>*/}
                            {/*    <div className="coupon-form-wrap">*/}
                            {/*        <form action="index.html">*/}
                            {/*            <p><input type="text" placeholder="Coupon" /></p>*/}
                            {/*            <p><input type="submit" value="Apply" /></p>*/}
                            {/*        </form>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}