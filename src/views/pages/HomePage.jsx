// import TeachingSection from '../components/TeachingSection'
// import DifferencesSection from '../components/DifferencesSection'
// import IntroSection from '../components/IntroSection'
// import TabsSection from '../components/TabsSection'
// import FeedBackSection from '../components/FeedBackSection'
// import { useState } from 'react'
// import EffectSection from '../components/EffectSection'

import {Link} from "react-router-dom";
import ThreeProducts from "../components/ThreeProducts";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import LogoCarousel from "../components/LogoCarousel.jsx";

export default function HomePage() {

  const images = [
    {

      id: 1,
      image: '/assets/img/hero-bg.jpg',
    },
    {
      id: 2,
      image: '/assets/img/hero-bg.jpg',
    },
    {
      id: 3,
      image: '/assets/img/hero-bg.jpg',
    },
    {
      id: 4,
      image: '/assets/img/hero-bg.jpg',
    },
  ]


  // const timer = false;
  return (
      <>

          <OwlCarousel
              className='owl-theme'
              items={1}
              dots={false}
              loop
              autoplay
          >
            {/*// <!-- single home slider -->*/}
            {
              images.map(image => (
                  // <img key={image.id} src={image.image} alt="" style={{height: '100vh', width: '100%'}}/>
                  <div className="single-homepage-slider item" key={image.id}
                       style={{backgroundImage: `url(${image.image})`}}>
                    <div className="container">
                      <div className="row">
                        <div className="col-md-12 col-lg-7 offset-lg-1 offset-xl-0">
                          <div className="hero-text" style={{marginTop: '30vh'}}>
                            <div className="hero-text-tablecell">
                              <p className="subtitle">Fresh & Organic</p>
                              <h1>Delicious Seasonal Fruits</h1>
                              <div className="hero-btns">
                                <Link to="shop.html" className="boxed-btn">Fruit Collection</Link>
                                <Link to="contact.html" className="bordered-btn">Contact Us</Link>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              ))
            }
            {/*// <!-- single home slider -->*/}
          </OwlCarousel>
        {/*// <!-- end hero area -->*/}

        {/*// <!-- features list section -->*/}
        <div className="list-section pt-80 pb-80">
          <div className="container">

            <div className="row">
              <div className="col-lg-4 col-md-6 mb-4 mb-lg-0">
                <div className="list-box d-flex align-items-center">
                  <div className="list-icon">
                    <i className="fas fa-shipping-fast"></i>
                  </div>
                  <div className="content">
                    <h3>Free Shipping</h3>
                    <p>When order over $75</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 mb-4 mb-lg-0">
                <div className="list-box d-flex align-items-center">
                  <div className="list-icon">
                    <i className="fas fa-phone-volume"></i>
                  </div>
                  <div className="content">
                    <h3>24/7 Support</h3>
                    <p>Get support all day</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6">
                <div className="list-box d-flex justify-content-start align-items-center">
                  <div className="list-icon">
                    <i className="fas fa-sync"></i>
                  </div>
                  <div className="content">
                    <h3>Refund</h3>
                    <p>Get refund within 3 days!</p>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        {/*// <!-- end features list section -->*/}

        {/*// <!-- product section -->*/}
        <ThreeProducts category={'random'}/>
        {/*// <!-- end product section -->*/}

        {/*// <!-- cart banner section -->*/}
        <section className="cart-banner pt-100 pb-100">
          <div className="container">
            <div className="row clearfix">
              {/*// <!--Image Column-->*/}
              <div className="image-column col-lg-6">
                <div className="image">
                  <div className="price-box">
                    <div className="inner-price">
                                <span className="price">
                                    <strong>30%</strong> <br/> off per kg
                                </span>
                    </div>
                  </div>
                  <img src="/assets/img/a.jpg" alt=""/>
                </div>
              </div>
              {/*// <!--Content Column-->*/}
              <div className="content-column col-lg-6">
                <h3><span className="orange-text">Deal</span> of the month</h3>
                <h4>Hikan Strwaberry</h4>
                <div className="text">Quisquam minus maiores repudiandae nobis, minima saepe id, fugit ullam similique!
                  Beatae, minima quisquam molestias facere ea. Perspiciatis unde omnis iste natus error sit voluptatem
                  accusant
                </div>
                {/*// <!--Countdown Timer-->*/}
                <div className="time-counter">
                  <div className="time-countdown clearfix" data-countdown="2020/2/01">
                    <div className="counter-column">
                      <div className="inner"><span className="count">00</span>Days</div>
                    </div>
                    <div className="counter-column">
                      <div className="inner"><span className="count">00</span>Hours</div>
                    </div>
                    <div className="counter-column">
                      <div className="inner"><span className="count">00</span>Mins</div>
                    </div>
                    <div className="counter-column">
                      <div className="inner"><span className="count">00</span>Secs</div>
                    </div>
                  </div>
                </div>
                <Link to="cart.html" className="cart-btn mt-3"><i className="fas fa-shopping-cart"></i> Add to
                  Cart</Link>
              </div>
            </div>
          </div>
        </section>
        {/*// <!-- end cart banner section -->*/}

        {/*// <!-- testimonail-section -->*/}
        <div className="testimonail-section mt-150 mb-150">
          <div className="container">

          </div>
        </div>
        {/*// <!-- end testimonail-section -->*/}

        {/*// <!-- advertisement section -->*/}
        <div className="abt-section mb-150">
          <div className="container">
            <div className="row">
              <div className="col-lg-6 col-md-12">
                <div className="abt-bg">
                  <Link to="https://www.youtube.com/watch?v=DBLlFWYcIGQ" className="video-play-btn popup-youtube"><i
                      className="fas fa-play"></i></Link>
                </div>
              </div>
              <div className="col-lg-6 col-md-12">
                <div className="abt-text">
                  <p className="top-sub">Since Year 1999</p>
                  <h2>We are <span className="orange-text">Fruitkha</span></h2>
                  <p>Etiam vulputate ut augue vel sodales. In sollicitudin neque et massa porttitor vestibulum ac vel
                    nisi. Vestibulum placerat eget dolor sit amet posuere. In ut dolor aliquet, aliquet sapien sed,
                    interdum velit. Nam eu molestie lorem.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente facilis illo repellat veritatis
                    minus, et labore minima mollitia qui ducimus.</p>
                  <Link to="about.html" className="boxed-btn mt-4">know more</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*// <!-- end advertisement section -->*/}

        {/*// <!-- shop banner -->*/}
        <section className="shop-banner">
          <div className="container">
            <h3>December sale is on! <br/> with big <span className="orange-text">Discount...</span></h3>
            <div className="sale-percent"><span>Sale! <br/> Upto</span>50% <span>off</span></div>
            <Link to="shop.html" className="cart-btn btn-lg">Shop Now</Link>
          </div>
        </section>
        {/*// <!-- end shop banner -->*/}

        {/*// <!-- latest news -->*/}
        <div className="latest-news pt-150 pb-150">
          <div className="container">

            <div className="row">
              <div className="col-lg-8 offset-lg-2 text-center">
                <div className="section-title">
                  <h3><span className="orange-text">Our</span> News</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, fuga quas itaque eveniet beatae
                    optio.</p>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-4 col-md-6">
                <div className="single-latest-news">
                  <Link to="single-news.html">
                    <div className="latest-news-bg news-bg-1"></div>
                  </Link>
                  <div className="news-text-box">
                    <h3><Link to="single-news.html">You will vainly look for fruit on it in autumn.</Link></h3>
                    <p className="blog-meta">
                      <span className="author"><i className="fas fa-user"></i> Admin</span>
                      <span className="date"><i className="fas fa-calendar"></i> 27 December, 2019</span>
                    </p>
                    <p className="excerpt">Vivamus lacus enim, pulvinar vel nulla sed, scelerisque rhoncus nisi.
                      Praesent vitae mattis nunc, egestas viverra eros.</p>
                    <Link to="single-news.html" className="read-more-btn">read more <i
                        className="fas fa-angle-right"></i></Link>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6">
                <div className="single-latest-news">
                  <Link to="single-news.html">
                    <div className="latest-news-bg news-bg-2"></div>
                  </Link>
                  <div className="news-text-box">
                    <h3><Link to="single-news.html">A mans worth has its season, like tomato.</Link></h3>
                    <p className="blog-meta">
                      <span className="author"><i className="fas fa-user"></i> Admin</span>
                      <span className="date"><i className="fas fa-calendar"></i> 27 December, 2019</span>
                    </p>
                    <p className="excerpt">Vivamus lacus enim, pulvinar vel nulla sed, scelerisque rhoncus nisi.
                      Praesent vitae mattis nunc, egestas viverra eros.</p>
                    <Link to="single-news.html" className="read-more-btn">read more <i
                        className="fas fa-angle-right"></i></Link>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 offset-md-3 offset-lg-0">
                <div className="single-latest-news">
                  <Link to="single-news.html">
                    <div className="latest-news-bg news-bg-3"></div>
                  </Link>
                  <div className="news-text-box">
                    <h3><Link to="single-news.html">Good thoughts bear good fresh juicy fruit.</Link></h3>
                    <p className="blog-meta">
                      <span className="author"><i className="fas fa-user"></i> Admin</span>
                      <span className="date"><i className="fas fa-calendar"></i> 27 December, 2019</span>
                    </p>
                    <p className="excerpt">Vivamus lacus enim, pulvinar vel nulla sed, scelerisque rhoncus nisi.
                      Praesent vitae mattis nunc, egestas viverra eros.</p>
                    <Link to="single-news.html" className="read-more-btn">read more <i
                        className="fas fa-angle-right"></i></Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 text-center">
                <Link to="news.html" className="boxed-btn">More News</Link>
              </div>
            </div>
          </div>
        </div>
        {/*// <!-- end latest news -->*/}

        {/*// <!-- logo carousel -->*/}
        <LogoCarousel/>
        {/*// <!-- end logo carousel -->*/}


      </>
  )
}

