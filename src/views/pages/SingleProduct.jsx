import Breadcrumb from "../components/Breadcrumb";
import {Link, useParams} from "react-router-dom";
import ThreeProducts from "../components/ThreeProducts";
import {useEffect, useState} from "react";
import {AddToCart} from "../components/AddToCart";
import {getRequest} from "../../hellper/db-request.js";

export default function SingleProduct(){
    const {id} = useParams();
    const [product, setProduct] = useState();

    const fetchProduct = async () => {
        const loadedLangs = await getRequest(`/shop/single/product/${id}`)
        setProduct(loadedLangs);
    };

    useEffect(() => {
        fetchProduct();
    }, [id])


    return(
        <>
            <Breadcrumb subtitle={'Fresh and Organic'} title={'Shop'} path={'/assets/img/hero-bg.jpg'}/>
            <div className="single-product mt-150 mb-150">
                <div className="container">
                    <div className="row">
                        <div className="col-md-5">
                            <div className="single-product-img">
                                <img src={product?.image} alt=""/>
                            </div>
                        </div>
                        <div className="col-md-7">
                            <div className="single-product-content">
                                <h3>{product?.title}</h3>
                                <p className="single-product-pricing">
                                    <span>Price</span>
                                    {product?.sale ?
                                        <>
                                             <span>
                                                <del>
                                                    {product?.price} ֏
                                                </del>
                                             </span>
                                             {product?.sale} ֏
                                        </>
                                        :
                                        `${product?.price} ֏`
                                    }
                                </p>
                                <p dangerouslySetInnerHTML={{ __html: product?.description }} />
                                <div className="single-product-form">
                                    {/*<form action="#">*/}
                                    {/*    <input type="number" placeholder="0"/>*/}
                                    {/*</form>*/}
                                    <AddToCart id={product?.id} />
                                    <p>
                                        <strong>Categories: </strong> {product?.category_name}
                                    </p>
                                </div>
                                <h4>Share:</h4>
                                <ul className="product-share">
                                    <li><Link to=""><i className="fab fa-facebook-f"></i></Link></li>
                                    <li><Link to=""><i className="fab fa-twitter"></i></Link></li>
                                    <li><Link to=""><i className="fab fa-google-plus-g"></i></Link></li>
                                    <li><Link to=""><i className="fab fa-linkedin"></i></Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ThreeProducts category={product?.category_id} />
        </>

    )
}