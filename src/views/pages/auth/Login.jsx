import {Link, useNavigate} from "react-router-dom";
import {postRequest} from "../../../hellper/db-request.js";
import {useForm} from "react-hook-form";
import {addLocalStorage, getLocalStorage} from "../../../hellper/hellper.js";
import {useEffect, useState} from "react";


export default function Login(){
    const {
        register,
        handleSubmit,
    } = useForm()

    const [message, useMessage] = useState('');
    const navigate = useNavigate()
    const authUser = getLocalStorage('access_token')


    useEffect(()=>{
        if (authUser?.length > 1){
            navigate("/profile")
        }
    }, [])



    const onSubmit = async (data) => {
        const login = await postRequest(`/login`, data)
        if (login.status){
            addLocalStorage('access_token', login.access_token)
            navigate("/profile")
        }else{
            useMessage(login.error)
        }
    };

    return(
        <>
            <main className="main-content  mt-0">
                <section>
                    <div className="page-header min-vh-75">
                        <div className="container">
                            <div className="row">
                                <div className="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
                                    <div className="card card-plain mt-8">
                                        <div className="card-header pb-0 text-left bg-transparent">
                                            <h3 className="font-weight-bolder text-info text-gradient">Welcome back</h3>
                                            <p className="mb-0">Enter your email and password to sign in</p>
                                        </div>
                                        <div className="card-body">
                                            <form role="form" onSubmit={handleSubmit(onSubmit)}>
                                                <label>Email</label>
                                                <div className="mb-3">
                                                    <input type="email"
                                                           className="form-control"
                                                           placeholder="Email"
                                                           id="Email"
                                                           aria-describedby="email-addon"
                                                           name='email'
                                                           {...register("email", {required: true})}
                                                        // onChange={event => setFormData((prev) => ({...prev, email:event.target.value}))}

                                                    />
                                                </div>
                                                <label>Password</label>
                                                <div className="mb-3">
                                                    <input type="password"
                                                           className="form-control"
                                                           placeholder="Password"
                                                           id="Password"
                                                           aria-describedby="password-addon"
                                                           name='password'
                                                           {...register("password", {required: true})}
                                                        // onChange={event => setFormData((prev) => ({...prev, password:event.target.value}))}
                                                    />
                                                </div>
                                                <span className="text-danger">{message ?? message}</span>
                                                <div className="text-center">
                                                    <button className="btn bg-gradient-info w-100 mt-4 mb-0">
                                                        Sign in
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <div className="card-footer text-center pt-0 px-lg-2 px-1">
                                            <p className="mb-4 text-sm mx-auto">
                                                Dont have an account?
                                                <Link to="/auth/register" className="text-info text-gradient font-weight-bold">
                                                    Register
                                                </Link>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                                        <div
                                            className="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6"
                                            style={{backgroundImage: "url('/profile/assets/img/curved-images/curved6.jpg')"}}></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </>
)
}