import OwlCarousel from "react-owl-carousel";

export default function LogoCarousel(){

    const images = [
        {

            id: 1,
            image: '/assets/img/company-logos/1.png',
        },
        {
            id: 2,
            image: '/assets/img/company-logos/2.png',
        },
        {
            id: 3,
            image: '/assets/img/company-logos/3.png',
        },
        {
            id: 4,
            image: '/assets/img/company-logos/4.png',
        },
        {
            id: 5,
            image: '/assets/img/company-logos/5.png',
        },
    ]


    return (
        <div className="logo-carousel-section">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="logo-carousel-inner">
                            <OwlCarousel
                                items={5}
                                autoplay
                                loop
                            >
                                {
                                    images.map(img => (
                                        <div className="single-logo-item item"  key={img.id} >
                                            <img src={img.image} alt=""/>
                                        </div>
                                    ))
                                }


                            </OwlCarousel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}