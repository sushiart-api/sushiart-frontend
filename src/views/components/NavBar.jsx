import {Link, NavLink} from "react-router-dom";
import {useEffect, useState} from 'react';
import {addLocalStorage, getLocalStorage} from "../../hellper/hellper.js";
import {getRequest} from "../../hellper/db-request.js";
import {GetDefLang} from "../../config/config-env.js";


export default function NavBar() {

    const [offset, setOffset] = useState(0);
    const [langs, setLangs] = useState();

    const localLangStorage = getLocalStorage("localization")

    const [localLang, setLocalLang] = useState(localLangStorage?.length ? localLangStorage : GetDefLang);
    const [localLangImage, setLocalLangImage] = useState();


    useEffect(() => {
        const onScroll = () => setOffset(window.scrollY);
        // clean up code
        window.removeEventListener('scroll', onScroll);
        window.addEventListener('scroll', onScroll, { passive: true });
        return () => window.removeEventListener('scroll', onScroll);
    }, []);


    const getData = async () => {
        const loadedLangs = await getRequest('/lang')
        setLangs(loadedLangs);
    };

    useEffect(() => {
        getData();
    }, [])

    const GetLocalStore = getLocalStorage("product_ids")

    function changeLangWeb(lang){
        language(lang);
        window.location.reload(true);
    }

    function language(lang){
        addLocalStorage('localization', lang)
        setLocalLang(lang)
        changeImageLang(lang)
    }

    useEffect(() => {
        language(localLang)
    }, [language]);


    function changeImageLang(lang){
        langs?.filter((item) => item.lang === lang && setLocalLangImage(item.path))
    }

    return (
        <>
            <div className={`sticky-wrapper ${offset > 0 && 'is-sticky'}`} >
                <div className={`top-header-area ${offset > 0 && 'top-header-area-fixed'}`}  id="sticker"
                     // style={offset > 0 && {position: 'fixed', background: 'rgb(5 25 34)'}}
                >
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-sm-12 text-center">
                                <div className="main-menu-wrap">
                                    {/*// <!-- logo -->*/}
                                    <div className="site-logo">
                                        <Link to="/">
                                            <img src="/assets/img/logo.png" alt=""/>
                                        </Link>
                                    </div>
                                    {/*// <!-- logo -->*/}

                                    {/*// <!-- menu start -->*/}
                                    <nav className="main-menu">
                                        <ul>
                                            <li><NavLink to="/">Home</NavLink></li>
                                            <li><NavLink to="/shop">Shop</NavLink></li>
                                            <li><NavLink to="/contact">Contact</NavLink></li>

                                            <li><NavLink to="/auth/login">login</NavLink></li>
                                            <li><NavLink to="/auth/register">register</NavLink></li>


                                            <li>
                                                <div className="header-icons">
                                                    <NavLink className="shopping-cart" to="/cart">
                                                        <span
                                                            className="style-shipping-card-count cookie-count-product ">{GetLocalStore.length}</span>
                                                        <i className="fas fa-shopping-cart"></i>
                                                    </NavLink>
                                                    <Link className="mobile-hide search-bar-icon" to="#">
                                                        <i className="fas fa-search"></i>
                                                    </Link>
                                                    <NavLink className="shopping-cart" to="/profile">
                                                        <i className="fa fa-user" aria-hidden="true"></i>
                                                    </NavLink>

                                                    <span className="dropdown-hover">
                                                        <Link to="#" className="mobile-and-display">
                                                            <span> {localLang} </span>
                                                            <img src={localLangImage} alt={localLang}/>
                                                        </Link>
                                                        <ul className="sub-menu">
                                                            {
                                                                langs?.map((lang) => (
                                                                    <li key={lang?.id}>
                                                                        <Link to='#'
                                                                              onClick={() => changeLangWeb(lang.lang, lang.path)}
                                                                              className={localLang === lang.lang ? 'active' : ''}>
                                                                            <img className="img-lang"
                                                                                 src={lang?.path} alt=""/>
                                                                            <span
                                                                                className="name-lang">{lang?.name}</span>
                                                                        </Link>
                                                                    </li>
                                                                ))
                                                            }
                                                        </ul>
                                                    </span>
                                                </div>
                                            </li>

                                        </ul>
                                    </nav>
                                    <Link className="mobile-show search-bar-icon" to="#">
                                        <i className="fas fa-search"></i>
                                    </Link>
                                    <div className="mobile-menu"></div>
                                    {/*// <!-- menu end -->*/}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/*// <!-- end header -->*/}

            {/*// <!-- search area -->*/}
            <div className="search-area">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <span className="close-btn"><i className="fas fa-window-close"></i></span>
                            <div className="search-bar">
                                <div className="search-bar-tablecell">
                                    <h3>Search For:</h3>
                                    <input type="text" placeholder="Keywords"/>
                                    <button type="submit">Search <i className="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}