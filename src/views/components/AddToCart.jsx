import {Link} from "react-router-dom";
import {useEffect, useState} from "react";
import {addCountCart, getLocalStorage} from "../../hellper/hellper.js";

export function AddToCart({id}){

    function addLocalStorage(productId){
        addCountCart(productId)
        inCartFunction()
    }

    const [inCart, setInCart] = useState();
    function inCartFunction(){
        const GetLocalStore = getLocalStorage("product_ids")
        const countOfTwo = GetLocalStore.filter(item => item === id).length;
        setInCart(countOfTwo)
    }

    useEffect(()=>{
        inCartFunction()
    }, [])

    return(
        <Link to="#" onClick={() => addLocalStorage(id)} className="cart-btn">
            <i className="fas fa-shopping-cart"></i>
            {
                inCart > 0 ? `In Cart [ ${inCart} ]` : 'Add to Cart'
            }
        </Link>
    )
}