import {useState, useEffect, useCallback} from "react";

export function PreLoader(){
    const [time, setTime] = useState(false);

    setTimeout(()=>{
        setTime(true);
    }, 1000)

    return(
        <>
            <div className="loader" style={time ? {display:'none'} : {}}>
                <div className="loader-inner">
                    <div className="circle"></div>
                </div>
            </div>
        </>
    )
}

export function PreLoaderProduct() {
    return (
        <div className="loader-inner-product">
            <div className="circle-product"></div>
        </div>
    )
}