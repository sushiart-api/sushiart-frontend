import {Link, NavLink} from "react-router-dom";
import { useEffect, useState } from 'react';


export default function SideBar() {

    return (
        <>
            <div className="container position-sticky z-index-sticky top-0">
                <div className="row">
                    <div className="col-12">
                        <nav
                            className="navbar navbar-expand-lg blur blur-rounded top-0 z-index-3 shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
                            <div className="container-fluid">
                                <Link className="navbar-brand font-weight-bolder ms-lg-0 ms-3 "
                                      to="/">
                                    Go to Home Page
                                </Link>
                                <button className="navbar-toggler shadow-none ms-2" type="button"
                                        data-bs-toggle="collapse"
                                        data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false">
                                  <span className="navbar-toggler-icon mt-2">
                                    <span className="navbar-toggler-bar bar1"></span>
                                    <span className="navbar-toggler-bar bar2"></span>
                                    <span className="navbar-toggler-bar bar3"></span>
                                  </span>
                                </button>
                                <div className="collapse navbar-collapse" id="navigation">
                                    <ul className="navbar-nav mx-auto">
                                        <li className="nav-item">
                                            <Link className="nav-link d-flex align-items-center me-2 active"
                                                  aria-current="page" to="/profile">
                                                <i className="fa fa-chart-pie opacity-6 text-dark me-1"></i>
                                                Dashboard
                                            </Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link me-2" to="/profile">
                                                <i className="fa fa-user opacity-6 text-dark me-1"></i>
                                                Profile
                                            </Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link me-2" to="/auth/register">
                                                <i className="fas fa-user-circle opacity-6 text-dark me-1"></i>
                                                Register
                                            </Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link className="nav-link me-2" to="/auth/login">
                                                <i className="fas fa-key opacity-6 text-dark me-1"></i>
                                                Login
                                            </Link>
                                        </li>
                                    </ul>
                                    <ul className="navbar-nav d-lg-block d-none">
                                        <li className="nav-item">
                                            <Link to="https://www.creative-tim.com/product/soft-ui-dashboard"
                                                  className="btn btn-sm btn-round mb-0 me-1 bg-gradient-dark">Free
                                                download</Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </>
    )
}