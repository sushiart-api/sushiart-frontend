import {Link} from "react-router-dom";
import {useCallback, useEffect, useState} from "react";
import {AddToCart} from "./AddToCart.jsx";
import {getRequest} from "../../hellper/db-request.js";

export default function ThreeProducts(params){
    const [products, setProducts] = useState([]);

    const changeCategory = async () => {
        if (params.category !== undefined){
            const products = await getRequest(`/shop/products/${params.category}?limit=${params.limit ?? 3}`)
            setProducts(products)
        }
    };

    useEffect(() => {
        changeCategory()
    }, [params.category])

    return(
        <>
            <div className="product-section mt-150 mb-150">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 offset-lg-2 text-center">
                            <div className="section-title">
                                <h3><span className="orange-text">Our</span> Products</h3>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, fuga quas itaque
                                    eveniet beatae optio.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {
                            products.map((product) => (
                                <div key={product?.id} className="col-lg-4 col-md-6 text-center">
                                    <div className="single-product-item">
                                        <Link to={`/product/${product.id}`}>
                                            <div className="product-image">
                                                <img src={product.image} alt=""/>
                                            </div>
                                            <h3>{product?.title}</h3>
                                        </Link>
                                        <p className="product-price">
                                            {product?.sale ?
                                                <>
                                                     <span>
                                                        <del>
                                                            {product?.price} ֏
                                                        </del>
                                                     </span>
                                                    {product?.sale} ֏
                                                </>
                                                :
                                                `${product?.price} ֏`
                                            }

                                        </p>
                                        <AddToCart id={product.id} />
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </>
    )
}