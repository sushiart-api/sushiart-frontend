export default function Button({ children, isActive, ...props}) {

    return (

        <button
            {...props}
             className="btn bg-gradient-info w-100 mt-4 mb-0">
            {children}
        </button>
    )
}