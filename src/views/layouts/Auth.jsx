import { Outlet } from 'react-router-dom'
import NavBar from "../components/auth/NavBar.jsx";
import Footer from "../components/auth/Footer.jsx";
import {PreLoader} from "../components/PreLoader.jsx";
import {importCssBasedOnPathname} from "../../config/package-style.js";

export default function Auth(){
    importCssBasedOnPathname()
    return(
        <>
            <PreLoader/>
            <NavBar/>
                <Outlet />
            <Footer/>
        </>
    )
}