import { Outlet } from 'react-router-dom'
import {importCssBasedOnPathname} from "../../config/package-style.js";
import SideBar from "../components/profile/SideBar.jsx";
import NavBar from "../components/profile/NavBar.jsx";
import {PreLoader} from "../components/PreLoader.jsx";
import Footer from "../components/profile/Footer.jsx";
import {auth} from "../../middleware/auth.js";


export default function Profile(){
    importCssBasedOnPathname();
    auth('profile')
    return(
        <>
            <PreLoader/>
            <SideBar />
            <main className="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
                <NavBar/>
                <div className="container-fluid py-4">
                    <Outlet/>
                    <Footer />
                </div>
            </main>
        </>
)
}