import { Outlet } from 'react-router-dom'
import NavBar from "../components/NavBar.jsx";
import Footer from "../components/Footer.jsx";
import {PreLoader} from "../components/PreLoader.jsx";
import {importCssBasedOnPathname} from "../../config/package-style.js";

export default function Layout(){
    importCssBasedOnPathname()
    return(
        <>
            <PreLoader/>
            <NavBar />

            <Outlet />

            <Footer/>
        </>
    )
}