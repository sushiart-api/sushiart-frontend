import { Routes, Route } from 'react-router-dom'
import HomePage from '../views/pages/HomePage'
import Contact from '../views/pages/Contact.jsx'
import Shop from '../views/pages/Shop'
import SingleProduct from '../views/pages/SingleProduct'
import Layout from '../views/layouts/Layout.jsx'
import Auth from '../views/layouts/Auth.jsx'
import Cart from "../views/pages/Cart.jsx";
import Checkout from "../views/pages/Checkout.jsx";
import ProfileLayout from "../views/layouts/ProfileLayout.jsx";
import Profile from "../views/pages/profile/Profile.jsx";
import Account from "../views/pages/profile/Account.jsx";
import Order from "../views/pages/profile/Order.jsx";
import History from "../views/pages/profile/History.jsx";
import Login from "../views/pages/auth/Login.jsx";
import Register from "../views/pages/auth/Register.jsx";
import {auth} from "../middleware/auth.js";



export default function RouteWeb(){
    
    return(
      <>
        <Routes>
            <Route path="/" element={<Layout />}>
                <Route index element={<HomePage />} />
                <Route path="/contact" element={<Contact />} />
                <Route path="/shop" element={<Shop />} />
                <Route path="/product/:id" element={<SingleProduct />} />
                <Route path="/cart" element={<Cart />} />
                <Route path="/checkout" element={<Checkout />} />
            </Route>

            <Route path="/profile"  element={<ProfileLayout />} >
                <Route index element={<Profile />} />
                <Route path="/profile/order" element={<Order />} />
                <Route path="/profile/history" element={<History />} />
                <Route path="/profile/account" element={<Account />} />

                <Route path="/profile/login" element={<Login />} />
            </Route>

            <Route path="/auth" element={<Auth />}>
                <Route path="/auth/login" element={<Login />} />
                <Route path="/auth/register" element={<Register />} />
            </Route>
        </Routes>
      </>
    )
  }