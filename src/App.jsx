import RouteWeb from './routes/RouteWeb.jsx'
import { BrowserRouter } from 'react-router-dom'

export default function App() {

  return (
    <>
      <BrowserRouter>
        <RouteWeb />
      </BrowserRouter>
    </>
  )
}

